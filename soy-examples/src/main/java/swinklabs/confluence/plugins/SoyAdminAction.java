package swinklabs.confluence.plugins;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.xwork.ActionViewDataMappings;

public class SoyAdminAction extends ConfluenceActionSupport {

	private static final long serialVersionUID = -6140912253712052047L;

	// soy parameter map
	private final Map<String, Object> soyContext = new HashMap<String, Object>();

	@Override
	public String execute() throws Exception {
		soyContext.put("name", "Mr. Tofu");
		return SUCCESS;
	}

	@ActionViewDataMappings
	public Map<String, Object> getData() {
		return soyContext;
	}

}
